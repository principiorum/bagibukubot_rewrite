const mysql = require('mysql');

const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'bagibuku'
});


module.exports.getMostDownloaded = (callback, limit) => {
	const query = `SELECT judul_buku, pengarang, count(id_buku) as jumlah 
		FROM downloaded 
		GROUP BY id_buku 
		ORDER BY jumlah DESC LIMIT ${limit}, 5`;
	connection.query(query, (error, results) => {
		let text = '';
		let buttonNumber = [];
		if (error) throw error;
		for (let i = 0; i < results.length; i++) {
			text += `${i+1+limit}. ${results[i].judul_buku}, ${results[i].pengarang}, *Hits: ${results[i].jumlah}*\n`;
			buttonNumber.push(i+1+limit);
		}
		
		callback(text, buttonNumber);
	});
};
/*
module.exports.downloadMostDownloaded = (callback, limit) => {
	const query = `SELECT id_buku, link_buku
		FROM data_buku 
		GROUP BY id_buku 
		ORDER BY jumlah DESC LIMIT ${limit}, 5`;
	connection.query(query, (error, results) => {
		let text = '';
		let buttonNumber = [];
		if (error) throw error;
		for (let i = 0; i < results.length; i++) {
			text += `${i+1+limit}. ${results[i].judul_buku}, ${results[i].pengarang}, *Hits: ${results[i].jumlah}*\n`;
			buttonNumber.push(i+1+limit);
		}
		
		callback(text, buttonNumber);
	});
};
*/
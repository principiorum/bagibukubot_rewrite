const { 
	informationButtonMarkup,
	homeButtonMarkup,
	bookButtonMarkup,
	hotBooksButtonMarkup 
} = require('../components/button');

const { 
	MostDownloadedButton
} = require('../components/inlineButton');

const {
	getMostDownloaded,
} = require('../database/db');

/**
 * fungsi ini untuk mendengar semua percakapan yang terjadi di bot yang berupa pesan
 * pola -> .on(event, listener)
 * some of event
 * text, audio, document, photo, sticker, video, voice, contact, location, new_chat_members, left_chat_member
 */
module.exports.textHandler = (msg, bot) => {
	switch(true) {
		/**
		 * /START
		 * Initial bot
		 * setelah start dipencet akan muncul daftar genre buku
		 * pastikan ada arahan untuk ketik/pencet /start diawal inisiasi bot
		 */
		case msg.text === '/start':
			bot.sendMessage(
				msg.chat.id, 
				'Welcome to *BagiBuku bot* \nA place to downloads many good books \u{270A}', 
				homeButtonMarkup
			);
			break;

		/**
		 * /DAFTAR BUKU
		 * setelah menu daftar buku dipencet akan muncul tombol2 sesuai genre yg tersedia
		 */
		case msg.text === '\u{1F4DA} Daftar Buku':
			bot.sendMessage(
				msg.chat.id, 
				'Silakan pilih kategori buku', 
				bookButtonMarkup
			);
			break;

		/**
		 * /BACK BUTTON
		 * tombol ini untuk kembali ke menu utama
		 */
		case msg.text === '\u{1F519} Back':
			bot.sendMessage(
				msg.chat.id, 
				'\u{1F519} Back', 
				homeButtonMarkup
			);
			break;

		/**
		 * /HOT BOOKS BUTTON
		 * tombol ini untuk menuju menu pilihan hot books
		 */
		case msg.text === '\u{1F525} Hot Books':
			bot.sendMessage(
				msg.chat.id, 
				'Pilihan buku-buku terbaik untuk anda', 
				hotBooksButtonMarkup
			);
			break;

		/**
		 * /DOWNLOAD TERBANYAK
		 * tampilkan jumlah download terbanyak
		 */
		case msg.text === '\u{1F4AF} Download Terbanyak':
			getMostDownloaded((data, limit) => {
				bot.sendMessage(
				msg.chat.id, 
				`*5/25 Buku Paling sering di download*\n\n${data}`,
				MostDownloadedButton(limit));
			}, 0);
			break;

		/**
		 * /INFORMATION BUTTON
		 * tombol ini untuk menuju menu informasi tentang bot
		 */
		case msg.text === '\u{1F4CC} Informasi':
			bot.sendMessage(
				msg.chat.id, 
				'Sekelumit informasi tentang *BagiBuku bot*. Kritik atau saran dapat disampaikan lewat fitur \'\u{1F4EA} Saran & Pesan\' di menu utama', 
				informationButtonMarkup
			);
			break;

		/**
		 * /CARI BUKU OR PENGARANG
		 * tombol ini untuk menuju menu pencarian buku
		 */
		case msg.text === '\u{1F50E} Cari Buku':
			bot.sendMessage(
				msg.chat.id, 
				'Masukkan judul buku atau nama pengarang yang kamu ingin cari, awali dengan tanda "+".\n\n*Misal* +leo tolstoy',
				{
					parse_mode: 'Markdown',
				}
			);
			break;

		/**
		 * /RESPONSE CARI BUKU
		 * hasil pencarian buku dan pengarang 
		 */
		case msg.text.charAt(0) === '+':
			const title = msg.text.substring(1).toLowerCase();

			if (title === 'mio') {
				bot.sendMessage(
					msg.chat.id, 
					'Buku ditemukan',
					homeButtonMarkup,
				);
			} else {
				bot.sendMessage(
					msg.chat.id, 
					'Judul buku atau pengarang yang kamu cari tidak ditemukan'
				);
			}
			break;

		/**
		 * /SARAN DAN PESAN
		 * menampilkan info cara menyampaikan saran dan pesan
		 */
		case msg.text === '\u{1F4EA} Saran & Pesan':
			bot.sendMessage(
				msg.chat.id, 
				'Sampaikan saran dan pesanmu untuk kami, awali pesan dengan tanda "-" dan minimal panjang pesan 100 karakter.\n\n*Misal* -pesan',
				{
					parse_mode: 'Markdown'
				}
			);
			break;

		/**
		 * /RESPONSE KASIH SARAN DAN PESAN
		 * apakah saran dan pesan diterima
		 */
		case msg.text.charAt(0) === '-':
			const pesan = msg.text.substring(1).toLowerCase();

			if (pesan.length > 99) {
				bot.sendMessage(
					msg.chat.id, 
					'Terimakasih untuk saranmu, semoga kami bisa meningkatkan service bot ini \u{1F601}',
					homeButtonMarkup,
				);
			} else {
				bot.sendMessage(
					msg.chat.id, 
					'Saranmu belum masuk ke database, pastikan ketentuan pembuatan saran dan pesan terpenuhi',
				);
			}
			break;

		/**
		 * /KONTRIBUSI
		 * arahan untuk ikut berkontribusi
		 */
		case msg.text === '\u{1F4E4} Kontribusi':
			bot.sendMessage(
				msg.chat.id, 
				`Bagi saudara/saudari budiman yang ingin membagikan file Ebook miliknya, berikut beberapa keterangan

1. Berbahasa Inggris atau Indonesia(diutamakan)
2. Type file PDF, Epub, DJVU, dll
3. File tidak 'Corrupt'.
4. kategori buku bebas (proses pemilahan dilakukan oleh kami)

Semua file yang akan dibagikan bisa di upload ke google drive, lalu kirim link-nya lewat menu \u{1F4EA} Saran & Pesan`,
				{
					parse_mode: 'Markdown'
				}
			);
			break;

		/**
		 * /DEFAULT RESPONSE
		 * jika bot menerima command yang tdk terdaftar akan memberikan default response
		 */
		default:
			bot.sendMessage(
				msg.chat.id, 
				'Bot tidak paham maksudmu \u{1F622}',
				homeButtonMarkup,
			);
	}
};

const { 
	MostDownloadedButton
} = require('../components/inlineButton');

const {
	getMostDownloaded,
} = require('../database/db');

module.exports.callbackHandler = (callback, bot) => {
	const callbackData = callback.data.split(' ');

	switch(callbackData[0]) {
		/**
		 * /HANDLE CALLBACK UNTUK NEXT MOST DOWNLOADED BOOKS
		 * @currentNum untuk get current showed books
		 * disini currentNum ditambahin 5 sebagai identifier current page
		 * ah coba ada state and redux hha
		 */
		case 'next':
			const currentNum = parseInt(callback.message.text.split('/', 1)[0]);
			if (currentNum < 25) {
				// fungsi ini untuk mendapatkan detail buku sesuai nomor
				getMostDownloaded((data, buttonNumber) => {
					bot.editMessageText(`*${currentNum+5}/25 Buku Paling sering di download*\n\n${data}`, {
						chat_id: callback.message.chat.id,
						message_id: callback.message.message_id,
						...MostDownloadedButton(buttonNumber)
					})
				}, currentNum)
			} else {
				// pesan atau alert ketika user tetap pencet padahal dah max
				bot.answerCallbackQuery(callback.id, {
					text: 'Kamu berada di akhir halaman'
				});
			}
			break;

		/**
		 * /HANDLE CALLBACK UNTUK PREV MOST DOWNLOADED BOOKS
		 * @currentNum untuk get current showed books
		 * disini currentNum dikurangin 5 sebagai identifier current page
		 */
		case 'prev':
			const currentNumPrev = parseInt(callback.message.text.split('/', 1)[0]);
			if (currentNumPrev > 5) {
				// fungsi ini untuk mendapatkan detail buku sesuai nomor
				getMostDownloaded((data, buttonNumber) => {
					bot.editMessageText(`*${currentNumPrev-5}/25 Buku Paling sering di download*\n\n${data}`, {
						chat_id: callback.message.chat.id,
						message_id: callback.message.message_id,
						...MostDownloadedButton(buttonNumber)
					})
				}, currentNumPrev-10)
			} else {
				// pesan atau alert ketika user tetap pencet padahal dah max
				bot.answerCallbackQuery(callback.id, {
					text: 'Kamu berada di awal halaman'
				});
			}
			break;

		/**
		 * /HANDLE CALLBACK UNTUK FIRST MOST DOWNLOADED BOOKS
		 * @currentNum untuk get current showed books
		 * disini currentNum diganti dengan first page
		 */
		case 'first':
			const currentNumFirst = parseInt(callback.message.text.split('/', 1)[0]);
			if (currentNumFirst > 5) {
				// fungsi ini untuk mendapatkan detail buku sesuai nomor
				getMostDownloaded((data, buttonNumber) => {
					bot.editMessageText(`*5/25 Buku Paling sering di download*\n\n${data}`, {
						chat_id: callback.message.chat.id,
						message_id: callback.message.message_id,
						...MostDownloadedButton(buttonNumber)
					})
				}, 0)
			} else {
				// pesan atau alert ketika user tetap pencet padahal dah max
				bot.answerCallbackQuery(callback.id, {
					text: 'Kamu berada di awal halaman'
				});
			}

			break;

		/**
		 * /HANDLE CALLBACK UNTUK LAST MOST DOWNLOADED BOOKS
		 * @currentNum diganti last number possible
		 * alias 20
		 */
		case 'last':
			const currentNumLast = parseInt(callback.message.text.split('/', 1)[0]);
			if (currentNumLast < 25) {
				// fungsi ini untuk mendapatkan detail buku sesuai nomor
				getMostDownloaded((data, buttonNumber) => {
					bot.editMessageText(`*25/25 Buku Paling sering di download*\n\n${data}`, {
						chat_id: callback.message.chat.id,
						message_id: callback.message.message_id,
						...MostDownloadedButton(buttonNumber)
					})
				}, 20)
			} else {
				// pesan atau alert ketika user tetap pencet padahal dah max
				bot.answerCallbackQuery(callback.id, {
					text: 'Kamu berada di akhir halaman'
				});
			}
			break;

		/**
		 * /HANDLE CALLBACK UNTUK FIRST BOOK MOST DOWNLOADED
		 */
		case 'book1':
			bot.sendMessage(callback.from.id, `Berhasil-${callbackData[1]}, Alhamdulillah`);
			break;

		/**
		 * /HANDLE CALLBACK UNTUK FIRST BOOK MOST DOWNLOADED
		 */
		case 'book2':
			bot.sendMessage(callback.from.id, `Berhasil-${callbackData[1]}, Alhamdulillah`);
			break;

		/**
		 * /HANDLE CALLBACK UNTUK FIRST BOOK MOST DOWNLOADED
		 */
		case 'book3':
			bot.sendMessage(callback.from.id, `Berhasil-${callbackData[1]}, Alhamdulillah`);
			break;

		/**
		 * /HANDLE CALLBACK UNTUK FIRST BOOK MOST DOWNLOADED
		 */
		case 'book4':
			bot.sendMessage(callback.from.id, `Berhasil-${callbackData[1]}, Alhamdulillah`);
			break;

		/**
		 * /HANDLE CALLBACK UNTUK FIRST BOOK MOST DOWNLOADED
		 */
		case 'book5':
			bot.sendMessage(callback.from.id, `Berhasil-${callbackData[1]}, Alhamdulillah`);
			break;
	}
}